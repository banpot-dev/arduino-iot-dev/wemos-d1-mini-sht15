/**
 * ReadSHT1xValues
 *
 * Read temperature and humidity values from an SHT1x-series (SHT10,
 * SHT11, SHT15) sensor.
 *
 * Copyright 2009 Jonathan Oxer <jon@oxer.com.au>
 * www.practicalarduino.com
 */

#include <SHT1x.h>

// Specify data and clock connections and instantiate SHT1x object
#define dataPin  D2
#define clockPin D1
SHT1x sht1x(dataPin, clockPin);

void setup()
{
   Serial.begin(115200); // Open serial connection to report values to host
   Serial.println("Starting up");
}

void loop()
{
  float temp_c;
  float temp_f;
  float humidity;
  float temperature;
  String temp_str, humidity_str;

  // Read values from the sensor
  temp_c = sht1x.readTemperatureC();
  temp_f = sht1x.readTemperatureF();
  humidity = sht1x.readHumidity();
  temperature = round(temp_c*100)/100.00;
  temp_str = String(temp_c, 2);
  humidity_str = String(humidity, 2);
  // Print the values to the serial port
  Serial.print("Temperature: ");
  Serial.print(temp_str);
  Serial.print(" *C. Humidity : ");
  Serial.print(humidity_str);
  Serial.println(" %");

  delay(2000);
}
