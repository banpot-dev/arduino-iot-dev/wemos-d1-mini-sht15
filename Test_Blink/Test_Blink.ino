// Test 2 LED ON Board
void setup() {
  // initialize digital pin D3 and D4 as an output.
  pinMode(D4, OUTPUT); // LED 1
  pinMode(D3, OUTPUT); // LED 2
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(D3, HIGH);   // turn the LED 1 on (HIGH is the voltage level)
  digitalWrite(D4, HIGH);   // turn the LED 2 on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(D3, LOW);    // turn the LED 1 off by making the voltage LOW
  digitalWrite(D4, LOW);    // turn the LED 2 off by making the voltage LOW
  delay(1000);                       // wait for a second
}
